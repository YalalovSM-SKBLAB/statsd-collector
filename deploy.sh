#!/bin/bash

# Get the deploy scripts
rm -rf ansible
wget --quiet -O ansible.zip 'http://192.168.15.198/artifactory/gen-snapshot-local/com/opsdatastore/opsdatastore-ansible/0.0.1-SNAPSHOT/opsdatastore-ansible-0.0.1-SNAPSHOT.zip'
unzip ansible.zip

RUNBOOK=ansible/statsd-collector-plugin.yml

# Acquire the collector ansible lock
while : ; do
  ansible-playbook -i ansible/inventories/$1 $RUNBOOK --tags=lock
  [[ $? != 0 ]] || break
  echo "Failed to acquire collector lock. Retrying"
done

for tag in 'common,preInstall' install postInstall unlock; do
  ansible-playbook -i ansible/inventories/$1 $RUNBOOK --tags=$tag
done
