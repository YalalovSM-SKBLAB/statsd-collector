# StatsD collector

# Steps to perform to start from scratch
- StatsD daemon configure as repeater.  
Create config.js and put the following config in it.  
```
    {
        port: 8125,
        mgmt_port: 8126,
        backends: [ "./backends/repeater" ],
        repeater: [ { host: 'localhost', port: 18125 } ],
        repeaterProtocol: ["udp4" ]
   }
```  

   You have to modify host and port which point to of statsd collector
        
-  Install 'generic-pool' module required by statsd  
 
    `npm install generic-pool`  

- Start statsD by command

    `node statsd.js config.js`  

# Now you can send metrics to statsd collector via statsd daemon

`echo "oracle.memory_usage:1|c" | nc -w 1 -u localhost 8125`


