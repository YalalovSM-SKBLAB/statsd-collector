package com.opsdatastore.collector.statsd;

import com.opsdatastore.collector.statsd.handlers.MetricsEventHandler;
import com.opsdatastore.collector.statsd.model.Application;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class StatsdCollectorTest {
	private final String METRIC_NAME_1 = "oracle.test";
	private final String METRIC_VALUE_1 = "1.1";
	private final String METRIC_TYPE_1 = "ms";
	private final String METRIC_RATE_1 = "2.0";
	private final String METRIC_DATA_1 = METRIC_NAME_1 + ":" + METRIC_VALUE_1 + "|" + METRIC_TYPE_1 + "|@" + METRIC_RATE_1;
	
	private final int TEST_PORT = 25829;
	private final int TIMEOUT_BEFORE_READ = 1000;
	
	Logger logger = LoggerFactory.getLogger("TestLogger");

	private MetricsEventHandler metricsEventHandler = null;
	private StatsdUdpSocketListener socketListener = null;

	private DatagramSocket sock;

	@Before
	public void init() throws Exception {
		BasicConfigurator.configure();
		metricsEventHandler = new MetricsEventHandler(logger);
		socketListener = new StatsdUdpSocketListener(metricsEventHandler, TEST_PORT, logger);
		socketListener.start();
		sock = new DatagramSocket();
	}

	@After
	public void tearDown() {
		socketListener.stop();
		metricsEventHandler = null;
		socketListener = null;
		sock.close();
	}

	@Test
	public void testSendReceive() {
		sendDatagram(METRIC_DATA_1);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		assertTrue(events.size() == 1);
	}

	@Test
	public void testParseMetricName() {
		sendDatagram(METRIC_DATA_1);

		try {
			Thread.sleep(TIMEOUT_BEFORE_READ);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		String metricName = events.get(0).getMetric().getMetricName();
		assertTrue(METRIC_NAME_1.equals(metricName));
	}

	@Test
	public void testParseMetricValue() {
		sendDatagram(METRIC_DATA_1);

		try {
			Thread.sleep(TIMEOUT_BEFORE_READ);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		Double metricValue = events.get(0).getMetric().getValue();
		Double difference = metricValue - Double.valueOf(METRIC_VALUE_1);
		
		assertTrue(Math.abs(difference) <= 0.000001);
	}

	@Test
	public void testParseMetricType() {
		sendDatagram(METRIC_DATA_1);

		try {
			Thread.sleep(TIMEOUT_BEFORE_READ);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		MetricType metricType = events.get(0).getMetric().getMetricType();
		assertTrue(metricType == MetricType.Timer);
	}

	@Test
	public void testParseMetricRate() {
		sendDatagram(METRIC_DATA_1);

		try {
			Thread.sleep(TIMEOUT_BEFORE_READ);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		Double metricRate = events.get(0).getMetric().getSamplingRate();
		Double difference = metricRate - Double.valueOf(METRIC_RATE_1);
		
		assertTrue(Math.abs(difference) <= 0.000001);
	}

	private void sendDatagram(String input) {
		byte[] content = input.getBytes();
		try {
			DatagramPacket datagramPacket = new DatagramPacket(content, content.length, InetAddress.getByName("localhost"), TEST_PORT);
			sock.send(datagramPacket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testTransformationLogic() {
		sendDatagram(METRIC_DATA_1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ignore) {}

		List<Event> events = metricsEventHandler.getAllEvents();
		logger.info("Received {} objects", events.size());
		MetricProcessor metricProcessor = new MetricProcessor();
		for (Event event : events) {
			Application app = metricProcessor.convertMetric(event);
			assertTrue(app.getName() != null );
		}
	}
}
