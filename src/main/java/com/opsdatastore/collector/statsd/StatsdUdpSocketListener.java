package com.opsdatastore.collector.statsd;

import com.opsdatastore.collector.statsd.handlers.MetricsEventHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;

public class StatsdUdpSocketListener {
    private Logger logger;
    private final Bootstrap bootStrap;
    private final NioEventLoopGroup nioEventLoopGroup = new NioEventLoopGroup();
	private final Channel channel;

    public StatsdUdpSocketListener(final MetricsEventHandler handler, final int listenOnPort, final Logger logger) {
		this.logger = logger;
        this.bootStrap = new Bootstrap()
				.channel(NioDatagramChannel.class)
				.group(nioEventLoopGroup)
				.localAddress(listenOnPort)
				.handler(new ChannelInitializer<NioDatagramChannel>() {
					@Override
					public void initChannel(NioDatagramChannel nioDatagramChannel) throws Exception {
						nioDatagramChannel.pipeline()
								.addLast("frameDecoder", new LineBasedFrameDecoder(80))
								.addLast("stringDecoder", new StringDecoder(CharsetUtil.UTF_8))
								.addLast(handler);
					}
				});

        this.channel = this.bootStrap.bind().syncUninterruptibly().channel();
	}

    public void start() {
		new Thread(() -> {
			try {
				channel.closeFuture().await();
			} catch (InterruptedException e) {
				logger.info("Channel listener interrupted", e);
			} finally {
				nioEventLoopGroup.shutdownGracefully();
			}
		});
    }

    public void stop() {
    	if (channel != null) {
    		channel.close();
		}
	}
}
