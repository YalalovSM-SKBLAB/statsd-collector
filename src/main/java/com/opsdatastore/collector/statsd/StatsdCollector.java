package com.opsdatastore.collector.statsd;

import com.codahale.metrics.health.HealthCheck;
import com.opsdatastore.annotation.CollectorExtension;
import com.opsdatastore.collector.CollectedObject;
import com.opsdatastore.collector.Collector;
import com.opsdatastore.collector.CollectorSink;
import com.opsdatastore.collector.statsd.handlers.MetricsEventHandler;
import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * This class is the implementation of the collector interface for gathering 
 * info from StatsD.  One of these is instantiated per monitored
 * instance in the config file
 */
@CollectorExtension(name = "StatsD Collector"
        , provider = "OpsDataStore" // Your or your organization's name
        , description = "Collector for StatsD" // This could be better
        , defaultCollectionIntervalSeconds = 60)
public class StatsdCollector implements Collector {
    private MetricsEventHandler metricsHandler;
    private StatsdUdpSocketListener listener;

    private Logger logger;

    // variable healthStatus changes it's state from healthy() to unhealthy()
    private HealthCheck.Result healthStatus = HealthCheck.Result.healthy();

    @Override
    public void setLogger(Logger logger) {
        this.logger = LoggerFactory.getLogger("StatsD logger");
    }

    /**
     * Check the health of this plugin
     * @return healthy or unhealthy with reason
     * @throws Exception never
     */
    @Override
    public HealthCheck.Result checkHealth() throws Exception {
        return healthStatus;
    }

    /**
     * This method is called right after the logger is set to configure the 
     * settings of the collector that are not specific to individual monitored 
     * instances.
     * @param config configuration object from the "settings" portion of the
     *               configuration file
     * @throws Exception on any error with configuring the collector
     */
    @Override
    public void configure(Config config) throws Exception {
        logger.info("Configuring StatsD collector plugin.");

        // Configure this collector based on common "settings" from 
        // application.conf
    }

    /**
     * This is called with the properties for the monitored instance that this
     * collector will be monitoring.  This is the entrypoint to the collector, 
     * and indicates that this collector should be started
     * @param instanceId Id of the instance
     * @param instanceProperties properties of the monitored instance
     */
    @Override
    public void start(String instanceId
    		, Properties instanceProperties) throws Exception {

        logger.info("Initializing StatsD collector " 
                + " for monitored instance " + instanceId);

        // Configure this collector based on the given monitored instance
        String listenOnPortProperty = instanceProperties.getProperty("listenOnPort");
        if (listenOnPortProperty == null) {
            String errorMessage = "Error starting StatsD collector plugin "
                    + instanceId
                    + ". Missing monitored instance property : "
                    + "listenOnPort";

            healthStatus = HealthCheck.Result.unhealthy(errorMessage);
            throw new RuntimeException(errorMessage);
        }

        int listenOnPort;

        try {
            listenOnPort = Integer.valueOf(listenOnPortProperty);
        } catch (NumberFormatException e) {
            String errorMessage = "Error starting StatsD collector plugin "
                    + instanceId
                    + ". Invalid instance property : "
                    + "listenOnPort = " + listenOnPortProperty
                    + ". An Integer is expected";

            healthStatus = HealthCheck.Result.unhealthy(errorMessage);
            throw new RuntimeException(errorMessage);
        }

        // Start processes associated with collection
		metricsHandler = new MetricsEventHandler(logger);
        listener = new StatsdUdpSocketListener(metricsHandler, listenOnPort, logger);
        listener.start();
    }

    /**
     * This is called to signal to the plugin that collection has stopped
     * throws Exception on any error stopping
     */
    @Override
    public void stop() throws Exception {
        // Handle stopping the collector
        if (listener != null) {
            listener.stop();
        }
    }

    /**
     * This is the heart of the collector; this method will be called regularly
     * to signal the collector to write information about monitored objects to
     * the sink
     * @param sink object to send collected updates and deletes 
     * @throws Exception on error
     */
    @Override
    public void collect(CollectorSink sink) throws Exception {
		logger.info("Collecting metrics started");
		List<Event> events = metricsHandler.getAllEvents();
        
		List<CollectedObject> objects = processEvents(events);
        for (CollectedObject obj : objects) {
        	sink.sendUpdate(obj);
        }
        logger.info("Collecting metrics finished");
    }

    private List<CollectedObject> processEvents(List<Event> events) {

        List<CollectedObject> result = new ArrayList<>();
        MetricProcessor metricProcessor = new MetricProcessor();
        for (Event event : events) {
            if(metricProcessor.convertMetric(event) != null){
                result.add(metricProcessor.convertMetric(event));
            }
        }
        return result;
    }
}
