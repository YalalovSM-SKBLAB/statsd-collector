package com.opsdatastore.collector.statsd;

import com.opsdatastore.collector.statsd.Event;
import com.opsdatastore.collector.statsd.StatsDMetric;
import com.opsdatastore.collector.statsd.model.Application;
import com.opsdatastore.collector.statsd.model.Component;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Alexey Toroschin  on 29.11.2017.
 */
public class MetricProcessor {

    public Application convertMetric(Event event) {
        Application application = null;
        StatsDMetric statsdMetric = event.getMetric();
        String metric = statsdMetric.getMetricName();
        // may be Exception java.lang.NullPointerException  if metric= null
        String[] metricSplit = metric.split("\\.");
        int i = metricSplit.length;

        switch(i) {
            case 0:
                // not real case
                break;
            case 1:
                // bad real case
                break;
            case 2:
                // case when metric in Application.class
                Application obj = new Application(metricSplit[0],event.getOrigin());
                obj.addMetric(metricSplit[1],statsdMetric.getValue());
                return obj;
                //break;
            default:
                // case when metric in Component.class
                Stream<String> streamFromArrays = Arrays.stream(metricSplit);
                String name = streamFromArrays.
                        limit(metricSplit.length-1).
                        skip(1).
                        collect(Collectors.joining("."));

                Application objC = new Application(metricSplit[0],event.getOrigin());
                Component component= new Component(name);
                String metricName = metricSplit[metricSplit.length-1];
                component.addMetric(metricName,statsdMetric.getValue());
                objC.addComponent(component);
                return objC;
        }
        return null;
    }

}
