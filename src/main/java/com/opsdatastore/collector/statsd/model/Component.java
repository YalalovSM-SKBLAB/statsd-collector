package com.opsdatastore.collector.statsd.model;

import com.opsdatastore.collector.CollectedComponent;
import com.opsdatastore.collector.ComponentType;

import java.util.Map;

/**
 * encapsulation of the the data from the Event
 */

@CollectedComponent.Type(ComponentType.PhysicalCpu)
public class Component extends CollectedComponent {

    private Map<String, Double> metrics;
    //private final Double value;

    public Component(String name) {
        super(name);
    }

    public void addMetric(String metricName, Double value) {
        metrics.put(metricName,value);
    }
}