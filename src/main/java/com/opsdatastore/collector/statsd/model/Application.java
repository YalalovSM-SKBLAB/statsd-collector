package com.opsdatastore.collector.statsd.model;

import com.opsdatastore.annotation.ID;
import com.opsdatastore.collector.CollectedObject;
import com.opsdatastore.collector.ObjectType;

import java.util.HashMap;
import java.util.Map;

@CollectedObject.Type(ObjectType.Application)
public class Application extends CollectedObject {

    @ID
    private final String hostname;
    private Map<String, Component> components;
    private Map<String, Double> metrics;

    public Application(String name, String hostname) {
        super(name);
        this.hostname = hostname;
        metrics = new HashMap<String, Double>();
    }

    public void addComponent(Component c) {
        components.put(c.getName(), c);
    }

    public void addMetric(String metricName, Double value) {
        metrics.put(metricName,value);
    }

}