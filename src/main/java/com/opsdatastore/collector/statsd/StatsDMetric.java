package com.opsdatastore.collector.statsd;

public class StatsDMetric {
    private final String metricName;                      // name of incoming metric
    private final MetricType metricType;                  // statsd metric type
    private final Double value;                           // value of the metric
    private final Double samplingRate;                          // for future purposes

    public StatsDMetric(String metricName, MetricType metricType, Double value, Double samplingRate) {
        this.metricName = metricName;
        this.metricType = metricType;
        this.value = value;
        this.samplingRate = samplingRate;
    }

	public String getMetricName() {
        return metricName;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public double getValue() {
        return value;
    }

    public double getSamplingRate() {
        return samplingRate;
    }
}
