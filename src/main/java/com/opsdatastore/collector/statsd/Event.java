package com.opsdatastore.collector.statsd;

import java.util.Date;

/**
 * Created by YalalovSM on 14.11.2017.
 */
public class Event {
	private final String origin;
	private final Date date;
	private final StatsDMetric metric;

	public Event(String origin, Date date, StatsDMetric metric) {
		this.origin = origin;
		this.date = date;
		this.metric = metric;
	}

	public String getOrigin() {
		return origin;
	}

	public Date getDate() {
		return date;
	}

	
	public StatsDMetric getMetric() {
		return metric;
	}
}
