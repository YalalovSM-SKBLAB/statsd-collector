package com.opsdatastore.collector.statsd;

public enum MetricType {
    Gauge,
    Counter,
    Timer,
    Set
}
