package com.opsdatastore.collector.statsd.handlers;

import com.opsdatastore.collector.statsd.Event;
import com.opsdatastore.collector.statsd.MetricType;
import com.opsdatastore.collector.statsd.StatsDMetric;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by YalalovSM on 09.11.2017.
 */
public class MetricsEventHandler extends SimpleChannelInboundHandler {
	private final int MAX_ELEMENTS_TO_DRAIN = 1024;
	private final BlockingQueue<Event> eventQueue = new LinkedBlockingDeque<>();
	private final Logger logger;

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
		logger.info("Received data on socket");
		DatagramPacket datagramPacket = (DatagramPacket)o;
		ByteBuf content = datagramPacket.content();

		byte[] bytes = new byte[content.readableBytes()];
		content.readBytes(bytes);
		String data = new String(bytes);
		Date now = new Date();
		
		String sourceAddress = datagramPacket.sender().getAddress().toString();
		int sourcePort = datagramPacket.sender().getPort();
		logger.debug("Data received from {}:{}, data={}", sourceAddress, sourcePort, data);
		
		String metricName = MetricHelper.getMetricName(data);
		if (metricName == null) return;
		
		MetricType metricType = MetricHelper.getMetricType(data);
		if (metricType == null) return;
		
		Double metricValue = MetricHelper.getMetricValue(data);
		if (metricValue == null) return;
		
		Double metricSamplingRate = MetricHelper.getMetricSamplingRate(data);
		if (metricSamplingRate == null) return;
		
		Event event = new Event(
				sourceAddress + ":" + sourcePort,
				now,
				new StatsDMetric(metricName,
						metricType,
						metricValue,
						metricSamplingRate)
		);

		eventQueue.add(event);
		logger.info("Stored data to queue");
	}

	
	public MetricsEventHandler(Logger logger) {
		this.logger = logger;
		this.logger.info("MetricsEventHandler started");
	}

	public List<Event> getAllEvents() {
		List<Event> result = new ArrayList<>();
		eventQueue.drainTo(result, MAX_ELEMENTS_TO_DRAIN);
		return result;
	}
	

}
