package com.opsdatastore.collector.statsd.handlers;

import com.opsdatastore.collector.statsd.MetricType;

public class MetricHelper {
    private final static String METRIC_PATTERN = "([a-z.\\-]+):([0-9.]+)\\|(\\w+)\\|@([0-9.]+)";

    public static String getMetricName(String input) {
    	return input.replaceAll(METRIC_PATTERN, "$1");
    }
    
    public static MetricType getMetricType(String input) {
    	MetricType metricType = null;
    	
    	final String metricAbbr = input.replaceAll(METRIC_PATTERN, "$3");
        switch (metricAbbr) {
            case "c":
                metricType = MetricType.Counter;
                break;
            case "g":
                metricType = MetricType.Gauge;
                break;
            case "ms":
                metricType = MetricType.Timer;
                break;
            case "s":
                metricType = MetricType.Set;
                break;
        }
    	return metricType;
    }
    
    public static Double getMetricValue(String input) {
    	String value = input.replaceAll(METRIC_PATTERN, "$2");
    	if (value == null)
    		return null;
    	else
    		return Double.valueOf(value);
    }
    
    public static Double getMetricSamplingRate(String input) {
    	String value = input.replaceAll(METRIC_PATTERN, "$4");
    	if (value == null)
    		return null;
    	else
    		return Double.valueOf(value);
    }
}
